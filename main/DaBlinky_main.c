/*
	DaBlinky - An esp32-based "IOT" bedside lamp using RGB LED strips (apa102). 

	v 0.0.3 - In this example we control 20 LEDs.
	We use a "Led" structure to describe one LED.
	We then use this structure to create an array of leds, which acts as a TX buffer.
*/

// Includes.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "driver/spi_master.h"
#include "driver/gpio.h"

// Pin definitions for SPI.
#define PIN_NUM_MISO -1
#define PIN_NUM_MOSI 13
#define PIN_NUM_CLK  14
#define PIN_NUM_CS   -1

// Structure to describe a single LED.
typedef struct Led Led;
struct Led{
	uint8_t brightness;
	uint8_t blue;
	uint8_t green;
	uint8_t red;
};

// Array of leds (one start frame, the 20 LEDs on the strip, one stop frame.
// The first ten LEDs are blue, the ten last are green.
DRAM_ATTR static Led leds[] = {
	// Start frame.
	{0x00, 0x00, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0xFF, 0x00, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xE3, 0x00, 0xFF, 0x00},
	{0xFF, 0xFF, 0xFF, 0xFF}
};

void app_main(void)
{
	printf("Hello world!\n");

	// Variables declarations.
	esp_err_t ret;
	spi_device_handle_t spi;

	// SPI bus configuration.
	spi_bus_config_t buscfg={
		.miso_io_num=PIN_NUM_MISO,
		.mosi_io_num=PIN_NUM_MOSI,
		.sclk_io_num=PIN_NUM_CLK,
	 	.quadwp_io_num=-1,
		.quadhd_io_num=-1,
		.max_transfer_sz=16
	};

	// SPI device configuration.
	spi_device_interface_config_t devcfg={
		.clock_speed_hz=1000*1000,	// Clock out at 1 MHz.
		.mode=0,			// SPI mode 0.
		.spics_io_num=PIN_NUM_CS,	// CS pin.
		.address_bits=0,		// No address.
		.command_bits=0,		// No command.
		.queue_size=1			// Max number of transactions we want to queue using spi_device_polling_transmit().
	};

	// Initialize the SPI bus.
	ret = spi_bus_initialize(HSPI_HOST, &buscfg, 2);
	ESP_ERROR_CHECK(ret);

	// Attach device to the SPI bus.
	ret = spi_bus_add_device(HSPI_HOST, &devcfg, &spi);
	ESP_ERROR_CHECK(ret);

	// SPI transaction description.
	static spi_transaction_t trans;
	// Not sure why the above descriptors need to be declared as static.
	// In SPI_master example it's stated that the SPI driver still needs to access theses descriptors
	// even when the function is finished, so using static prevents the descriptors to be allocated
	// on the stack.


	// Sending out data.
	memset(&trans, 0, sizeof(spi_transaction_t));
	trans.tx_buffer = &leds;
	trans.rx_buffer = NULL;
	trans.length = 704;

	ret = spi_device_queue_trans(spi, &trans, portMAX_DELAY);
	assert(ret == ESP_OK);
}
